import React from "react";

class Timer extends React.Component {
  intervalId = null;

  state = {
    count: 0,
    explodesAt: Number.parseInt(this.props.explodesAtInitial) || 5,
  };

  // Allows you to update your state from receiving new props
  static getDerivedStateFromProps() {
    return null;
  }

  // Third
  componentDidMount() {
    this.intervalId = setInterval(() => {
      this.setState((state) => ({
        count: state.count + 1,
      }));
    }, 1000);
  }

  // Did an update happen? 4th
  componentDidUpdate() {
    const { count, explodesAt } = this.state;
    if (count === explodesAt) {
      throw new Error("KABOOM");
    }
  }

  shouldComponentUpdate() {
    return true;
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
    console.log("I am the Timer and I am leaving! Goodbye");
  }

  // Second
  render() {
    const { count } = this.state;
    return <div>{count}</div>;
  }
}

export default Timer;
