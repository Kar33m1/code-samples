import React from "react";
import { Link, withRouter } from "react-router-dom";
import { List, Button, ButtonGroup } from "@fluentui/react-northstar";
import { CloseIcon } from "@fluentui/react-icons-northstar";

const actions = (
  <ButtonGroup
    buttons={[
      {
        icon: <CloseIcon />,
        iconOnly: true,
        text: true,
        title: "Close",
        key: "close",
      },
    ]}
  />
);

const formatList = (place) => ({
  key: place.id,
  // media: <Image src="public/images/avatar/small/matt.jpg" avatar />,
  header: place.location,
  headerMedia: "7:26:56 AM",
  content: "",
  contentMedia: "",
  endMedia: actions,
});

class SidebarComponent extends React.Component {
  state = {
    place: "",
  };
  onInputChange = (ev) => {
    const place = ev.target.value;
    this.setState(() => ({ place }));
  };

  submitForm = (ev) => {
    ev.preventDefault();
    this.props.submitForm(this.state.place);
    this.setState((state) => ({ place: "" }));
  };

  render() {
    const { place } = this.state;
    const { locations, removePlace } = this.props;
    return (
      <div style={{ display: "flex", flexDirection: "column" }}>
        <form onSubmit={this.submitForm}>
          <input name="place" value={place} onChange={this.onInputChange} />
          <Button type="submit" content="Submit" primary />
        </form>
        <p>You have typed: {place}</p>
        <List items={locations.map(formatList)} />
        {/* {locations.map((loc) => (
          <React.Fragment key={loc.id}>
            <Link to={"#"}>{loc.place}</Link>
            <a href="#" data-placeid={loc.id} onClick={removePlace}>
              x
            </a>
          </React.Fragment>
        ))} */}
      </div>
    );
  }
}

export const Sidebar = withRouter(SidebarComponent);

/*
export const Sidebar = () => {
  const [locations, setLocations] = useState([{ id: 0, place: 75650 }]);
  const [place, changePlace] = useState("");

  const submitForm = (ev) => {
    ev.preventDefault();
    alert("Woohoo!");
  };

  const onInputChange = (ev) => {
    changePlace(ev.target.value);
  };

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <form onSubmit={submitForm}>
        <input name="place" value={place} onChange={onInputChange} />
        <button type="submit">Submit</button>
      </form>
      <p>You have typed: {place}</p>
      {locations.map((loc) => (
        <Link key={loc.id} to={"#"}>
          {loc.place}
        </Link>
      ))}
    </div>
  );
};
*/
